(in-package :slime-jam)

(defun carga-mosaicos (ruta)
  (mapcan (lambda (archivo)
            (multiple-value-bind (tipo partes nombre-archivo)
                (uiop/pathname:split-unix-namestring-directory-components
                 (namestring archivo))
              (declare (ignore tipo partes))
              (list (alexandria:make-keyword
                     (string-upcase
                      (car (multiple-value-list (uiop/pathname:split-name-type nombre-archivo)))))
                    (clim:make-pattern-from-bitmap-file archivo))))
          (directory ruta)))

(defparameter *mosaicos*
  (carga-mosaicos (merge-pathnames "mosaicos/*.png"
                                   *ruta-del-sistema*)))

(defparameter *caracteres* '((#\Space . :nada)
                             (#\━ . :piso)
                             (#\─ . :piso)
                             (#\┃ . :piso)
                             (#\╞ . :tubo-horizontal)
                             (#\╼ . :tubo-horizontal-derecha-final)
                             (#\╾ . :tubo-horizontal-izquierda-final)
                             (#\╡ . :tubo-horizontal)
                             (#\┠ . :piso)
                             (#\┨ . :piso)
                             (#\╥ . :tubo-abajo)
                             (#\║ . :tubo-abajo)
                             (#\╽ . :tubo-abajo-final)
                             (#\┓ . :piso)
                             (#\┗ . :piso)
                             (#\┏ . :piso)
                             (#\┛ . :piso)))

(defun carga-escenarios (ruta &optional (caracteres *caracteres*))
  (labels ((interpreta-linea (linea)
             (loop for c across linea
                   collect (cdr (assoc c caracteres)))))
    (let ((archivos (directory ruta)))
      (mapcar (lambda (archivo)
                (let ((escenario (with-open-file (input archivo :direction :input)
                                   (loop for linea = (read-line input nil :eof)
                                         while (not (eq :eof linea))
                                         collect (interpreta-linea linea)))))
                  (make-array (list (length escenario) (length (car escenario)))
                              :initial-contents escenario :element-type 'keyword)))
              archivos))))

(defparameter *escenarios*
  (carga-escenarios (merge-pathnames "escenarios/*.txt"
                                     *ruta-del-sistema*)))

(defun dibuja-escenario (medio escenario mosaicos x y
                         &optional
                           (escenario-fondo *escenario-fondo*)
                           (escenario-ancho *escenario-ancho*)
                           (escenario-alto *escenario-alto*))
  (let* ((ancho-medio (bounding-rectangle-width (sheet-region medio)))
         (alto-medio (bounding-rectangle-height (sheet-region medio)))
         (ancho-mosaico (floor ancho-medio escenario-ancho))
         (alto-mosaico (floor alto-medio escenario-alto)))
    (draw-pattern* medio escenario-fondo 0 0)
    (dotimes (j escenario-alto)
      (dotimes (i escenario-ancho)
        (let ((tipo (aref escenario j i)))
          (unless (eq :nada tipo)
            ;;(draw-rectangle* medio (+ x (* i ancho-mosaico)) (+ y (* j alto-mosaico)) (+ x (* (1+ i) ancho-mosaico)) (+ y (* (1+ j) alto-mosaico)) :ink (getf mosaicos tipo))
            (draw-pattern* medio (getf mosaicos tipo) (+ x (* i ancho-mosaico)) (+ y (* j alto-mosaico)))))))))
