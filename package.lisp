;;;; package.lisp

(defpackage #:slime-jam
  (:use #:clim
        #:clim-lisp
        #:mcclim-render
        #:trivial-clim-sprites)
  (:export #:main
           #:slime-jam-entry-point))
