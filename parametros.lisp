(in-package #:slime-jam)

(declaim (type double-float *ancho-df* *alto-df* *ancho-df/2* *alto-df/2*))
(defparameter *ancho* 1024)
(defparameter *alto* 1024)
(defparameter *ancho/2* 512)
(defparameter *alto/2* 512)
(defparameter *ancho-df* (coerce *ancho* 'double-float))
(defparameter *alto-df* (coerce *alto* 'double-float))
(defparameter *ancho-df/2* (/ *ancho-df* 2.0d0))
(defparameter *alto-df/2* (/ *alto-df* 2.0d0))

(declaim (type double-float *max-x* *max-y* *min-x* *min-y*))
(defparameter *max-x* (/ *ancho* 2.0d0))
(defparameter *max-y* (/ *alto* 2.0d0))
(defparameter *min-x* (- (1- *max-x* )))
(defparameter *min-y* (- (1- *max-y* )))

(declaim (type double-float *desp-x* *pausa*))
(defparameter *pausa* (/ 25.0d0))
(defparameter *ruta-del-sistema* (asdf:component-pathname (asdf:find-system 'slime-jam)))

(defparameter *teclas* '())

;; Munching squares
(declaim (type double-float *ms-cuantos*))
(defparameter *ms-num-cuadros* 64.0d0)

;; Escenarios
(defparameter *escenario-ancho* (/ *ancho* 32))
(defparameter *escenario-alto* (/ *alto* 32))
(defparameter *escenario-fondo* (make-pattern-from-bitmap-file
                                 (merge-pathnames "sewergame.png"
                                                  *ruta-del-sistema*)))
