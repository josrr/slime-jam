(in-package #:slime-jam)

(defun munching-squares (pane)
  (declare (optimize (speed 3) (safety 1)))
  (draw-rectangle* pane 0 0 *ancho* *alto* :filled t :ink +black+)
  (loop with cuantos double-float = *ms-num-cuadros*
        with cuantos-1 fixnum = (round (1- cuantos))
        with total double-float = (* cuantos cuantos)
        with num-cuadro fixnum = (espacio-num-cuadro pane)
        with ancho double-float = (min (/ *ancho-df* cuantos) (/ *alto-df* cuantos))
        with ancho/2 double-float = (/ ancho 2.0d0)
        with cuantos-si double-float = 0.0d0
        for x from 0 to cuantos-1 and xmed double-float from ancho/2 by ancho do
          (loop for y from 0 to cuantos-1 and ymed double-float from ancho/2 by ancho
                if (< (logxor x y) num-cuadro) do
                  (draw-point* pane xmed ymed :ink +darkslategrey+ :line-thickness ancho)
                  (incf cuantos-si))
        finally (if (>= cuantos-si total)
                    (setf (espacio-num-cuadro pane) 1)
                    (incf (the fixnum (espacio-num-cuadro pane))))))

(defun jugar (pane)
  (let ((ancho (bounding-rectangle-width (sheet-region pane)))
        (alto (bounding-rectangle-height (sheet-region pane))))
    (when (null (espacio-datos pane))
      (crea-slime pane :jugador 64 (- alto 32))
      (crea-slime pane :enemigo 64 160)
      (crea-slime pane :enemigo 352 160)
      (crea-slime-vectorial pane :tipo-1 64 (- alto 192) 32)
      (setf (espacio-imagen pane)
            (mcclim-raster-image:with-output-to-rgba-pattern (stream :width ancho
                                                                     :height alto
                                                                     :border-width 0
                                                                     :recording-p nil)
              (draw-rectangle* stream 0 0 ancho alto :filled t :ink +black+)
              (dibuja-escenario stream (car *escenarios*) *mosaicos* 0 0)))
      (draw-design pane (espacio-imagen pane)))
    (dibuja-slimes pane)
    (dibuja-slimes-vectoriales pane)))

(defun dibuja-fondo (pane)
  (let ((ancho (bounding-rectangle-width (sheet-region pane)))
        (alto (bounding-rectangle-height (sheet-region pane))))
    (setf (espacio-imagen pane)
          (mcclim-raster-image:with-output-to-rgba-pattern (stream :width ancho
                                                                   :height alto
                                                                   :border-width 0
                                                                   :recording-p nil)
            (draw-rectangle* stream 0 0 ancho alto :filled t :ink +black+)
            (dibuja-escenario stream (car *escenarios*) *mosaicos* 0 0)))
    (mcclim-render:copy-image (espacio-imagen pane) 0 0 ancho alto
                              (espacio-buffer pane) 0 0)))

(defun jugar-2 (pane)
  (let ((ancho (bounding-rectangle-width (sheet-region pane)))
        (alto (bounding-rectangle-height (sheet-region pane))))
    (when (null (espacio-datos pane))
      (setf (espacio-imagen pane)
            (mcclim-raster-image:with-output-to-rgba-pattern (stream :width ancho
                                                                     :height alto
                                                                     :border-width 0
                                                                     :recording-p nil)
              (draw-rectangle* stream 0 0 ancho alto :filled t :ink +black+)
              (dibuja-escenario stream (car *escenarios*) *mosaicos* 0 0)))
                                        ;(draw-design pane (espacio-imagen pane))
      (updating-output (pane)
        (draw-rectangle* pane 0 0 (1- ancho) (1- alto) :ink (espacio-imagen pane)))
      (setf (espacio-datos pane) (list :x 0 :y 0)))
    ;;(draw-rectangle* pane )
    ))
