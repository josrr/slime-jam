(in-package #:slime-jam)

;;(declaim (type (simple-array (cons fixnum fixnum) *) *slime-vectorial*))

(defparameter *slimes-vectoriales*
  (list :tipo-1
        (make-array 8
                    :element-type '(cons fixnum fixnum)
                    :initial-contents '((0 . 0) (1 . 2.5) (2 . 3.5) (3 . 4) (4 . 4) (5 . 3.5) (6 . 2.5) (7 . 0)))))

(defun slime-vec-calcula-puntos (descripcion longitud)
  (let* ((y1 0)
         (y2 longitud)
         (dx (/ longitud 8.0d0))
         (puntos (loop for celda across descripcion
                       for yy = (- longitud (* dx (cdr celda)))
                       if (> yy y2)
                         do (setf y2 yy)
                       if (< yy y1)
                         do (setf y1 yy)
                       append (list (* dx (car celda)) yy))))
    (values puntos (- y2 y1))))

(defun slime-vec-regenera-imagen (slime puntos alto)
  (setf (getf slime :imagen)
        (mcclim-raster-image:with-output-to-rgba-pattern (stream :width (getf slime :longitud-fix)
                                                                 :height alto
                                                                 :border-width 0
                                                                 :recording-p nil)
          (draw-polygon* stream puntos :ink +light-green+ :filled t :line-thickness 0)
          (draw-polygon* stream puntos :ink +dark-green+ :filled nil :line-thickness 3))))

(defun slime-vec-cayendo (slime)
  (let ((dy -8))
    (if (> (+ (getf slime :y) dy) 128)
        (slime-vec-cambia-posicion slime (getf slime :x) (+ (getf slime :y) dy))
        (setf (getf slime :func) 'slime-vec-subiendo))))

(defun slime-vec-subiendo (slime)
  (let ((dy 8))
    (if (< (+ (getf slime :y) dy) (- *alto* (getf slime :alto)))
        (slime-vec-cambia-posicion slime (getf slime :x) (+ (getf slime :y) dy))
        (setf (getf slime :func) 'slime-vec-cayendo))))

(defun crea-slime-vectorial (pane tipo &optional
                                         (x 0.0d0) (y 0.0d0) (longitud 32.0d0)
                                         (func #'slime-vec-cayendo))
  (let ((descripcion (getf *slimes-vectoriales* tipo)))
    (when descripcion
      (let ((slime (list :x x
                         :y y
                         :x-anterior nil
                         :y-anterior nil
                         :alto-anterior nil
                         :longitud-anterior longitud
                         :alto nil
                         :imagen nil
                         :buffer nil
                         :longitud longitud
                         :longitud-fix (ceiling longitud)
                         :descripcion descripcion
                         :func (or func #'slime-vec-cayendo))))
        (multiple-value-bind (puntos alto) (slime-vec-calcula-puntos descripcion longitud)
          (slime-vec-regenera-imagen slime puntos alto)
          (let ((alto (ceiling alto)))
            (setf (getf slime :buffer) (make-image (getf slime :longitud-fix) alto)
                  (getf slime :alto) alto
                  (getf slime :alto-anterior) alto)))
        (push slime (getf (espacio-datos pane) :slimes-vectoriales))
        (dibuja-slime-vectorial pane slime)))))

(defun slime-vec-cambia-longitud (slime nueva-longitud)
  (setf (getf slime :longitud-anterior) (getf slime :longitud)
        (getf slime :alto-anterior) (ceiling (getf slime :alto))
        (getf slime :longitud) nueva-longitud
        (getf slime :longitud-fix) (ceiling nueva-longitud))
  (multiple-value-bind (puntos alto)
      (slime-vec-calcula-puntos (getf slime :descripcion) nueva-longitud)
    (slime-vec-regenera-imagen slime puntos alto)
    (setf (getf slime :alto) alto
          (getf slime :buffer) (make-image (getf slime :longitud-fix) (ceiling alto)))))

(defun slime-vec-cambia-posicion (slime nueva-x nueva-y)
  (setf (getf slime :x-anterior) (getf slime :x)
        (getf slime :y-anterior) (getf slime :y)
        (getf slime :x) nueva-x
        (getf slime :y) nueva-y))

(defun borra-slime-vectorial (pane slime)
  (let ((x (getf slime :x))
        (y (- *alto-df* (getf slime :y) (getf slime :alto)))
        (ancho (getf slime :longitud-fix))
        (alto (getf slime :alto)))
    (mcclim-render:blend-image (espacio-imagen pane) x y ancho alto (getf slime :buffer) 0 0)
    (draw-pattern* pane (getf slime :buffer) x y)))

(defun slime-vec-mueve (slime direccion cuando)
  t)

(defun borra-dy (pane slime dy fondo)
  (declare (optimize (speed 3)))
  (let* ((img-ancho (getf slime :longitud-anterior))
         (img-alto (ceiling (abs (- (getf slime :y) (getf slime :y-anterior)))))
         (img (make-image img-ancho img-alto))
         (yy (if (> dy 0)
                 (+ (getf slime :y-anterior) dy (- img-alto))
                 (getf slime :y-anterior))))
    (declare (type fixnum img-ancho img-alto dy))
    (mcclim-render:blend-image fondo
                               (getf slime :x-anterior) (- *ancho* yy img-alto)
                               img-ancho img-alto img 0 0)
    (draw-pattern* pane img
                   (getf slime :x-anterior) (- *ancho* yy img-alto))
    (setf (getf slime :y-anterior) (getf slime :y))))

(defun borra-dx (pane slime dx fondo)
  (declare (optimize (speed 3)))
  (let* ((img-ancho (ceiling (abs (- (getf slime :x) (getf slime :x-anterior)))))
         (img-alto (getf slime :alto-anterior))
         (img (make-image img-ancho img-alto))
         (xx (if (> dx 0)
                 (+ (getf slime :x-anterior) dx (- img-ancho))
                 (getf slime :x-anterior))))
    (declare (type fixnum img-ancho img-alto dx))
    (mcclim-render:blend-image fondo
                               xx (- *ancho* (getf slime :y-anterior) img-alto)
                               img-ancho img-alto img 0 0)
    (draw-pattern* pane img
                   xx
                   (- *alto-df* (getf slime :y-anterior) img-alto))
    (setf (getf slime :x-anterior) (getf slime :x))))

(defun dibuja-slime-vectorial (pane slime)
  (when (or (null (getf slime :x-anterior))
            (/= (getf slime :x-anterior) (getf slime :x))
            (/= (getf slime :y-anterior) (getf slime :y)))
    (let ((x (getf slime :x))
          (y (- *alto-df* (getf slime :y) (getf slime :alto)))
          (ancho (getf slime :longitud))
          (alto (getf slime :alto)))
      (labels ((dibuja ()
                 ;;(mcclim-render:blend-image (espacio-imagen pane) x y ancho alto (getf slime :buffer) 0 0)
                 ;;(mcclim-render:blend-image (getf slime :imagen)  0 0 ancho alto (getf slime :buffer) 0 0)
                 (mcclim-render:blend-image (getf slime :imagen)  0 0 ancho alto (espacio-buffer pane) x y)
                 ;;(draw-pattern* pane (getf slime :buffer) x y)
                 ))
        (setf (getf slime :x-anterior) (getf slime :x)
              (getf slime :y-anterior) (getf slime :y))
        (dibuja)
        ;; (when (/= (getf slime :x) (getf slime :x-anterior))
        ;;   (if (>= (getf slime :x) (getf slime :x-anterior))
        ;;       (borra-dx pane slime 0 (espacio-imagen pane))
        ;;       (borra-dx pane slime (getf slime :longitud-anterior) (espacio-imagen pane))))
        ;; (when (/= (getf slime :y) (getf slime :y-anterior))
        ;;   (if (>= (getf slime :y) (getf slime :x-anterior))
        ;;       (borra-dy pane slime 0 (espacio-imagen pane))
        ;;       (borra-dy pane slime (getf slime :alto-anterior) (espacio-imagen pane))))
        ))))

(defun dibuja-slimes-vectoriales (pane)
  ;; (dolist (slime )
  ;;   (when (getf slime :func)
  ;;     (funcall (getf slime :func) slime)))
  (mapcar (lambda (slime)
            (dibuja-slime-vectorial pane slime))
          (getf (espacio-datos pane) :slimes-vectoriales)))
