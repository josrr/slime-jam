;;;; slime-jam.lisp
(in-package #:slime-jam)

(defun anima (frame)
  (lambda ()
    (loop with pane = (find-pane-named frame 'espacio-pane)
          while (bt:with-lock-held ((slime-jam-bloqueo frame)) (null (espacio-terminar pane)))
            initially (presentacion pane)
          do (bt:with-lock-held ((slime-jam-bloqueo frame))
               (unless (espacio-terminar pane)
                 (funcall (espacio-animacion-func pane) pane)))
             (bt:thread-yield)
             (sleep *pausa*))))


(defparameter *slime-jam* nil)
(defun inicia ()
  (let* ((teclas (make-hash-table))
         (frame (make-application-frame 'slime-jam
                                        :teclas teclas)))
    (setf *slime-jam* frame)
    (run-frame-top-level frame)
    (setf *slime-jam* nil)
    (when (espacio-pixmap (find-pane-named frame 'espacio-pane))
      (clim:deallocate-pixmap (espacio-pixmap (find-pane-named frame 'espacio-pane))))
    (let ((hilo (slime-jam-hilo-animacion frame)))
      (when (and hilo (bt:thread-alive-p hilo)
                 (bt:destroy-thread hilo)))))
  :name "Slime-Jam")

(defun salir ()
  #+sbcl (sb-ext:quit)
  #+ecl (ext:quit)
  #+clisp (ext:exit)
  #+ccl (ccl:quit)
  #-(or sbcl ecl clisp ccl) (cl-user::quit))

(defun main (&rest arguments)
  (declare (ignore arguments))
  (bt:join-thread (bt:make-thread #'inicia :name "slime-jam"))
  (unless (or (find :slynk *features*) (find :swank *features*))
    (salir)))


(defclass espacio-pane (clime:never-repaint-background-mixin
                        ;;clim-stream-pane
                        basic-gadget)
  ((x :initform (- 4096 *ancho*) :accessor espacio-x)
   (y :initform 0 :accessor espacio-y)
   (fondo :initform +black+
          :reader espacio-color-de-fondo)
   (pixmap :initform nil :accessor espacio-pixmap)
   (imagen :initform nil :accessor espacio-imagen)
   (buffer :initform (make-image *ancho* *alto*) :accessor espacio-buffer)
   (objetos :initform nil :accessor espacio-objs)
   (num-cuadro :initform 1 :accessor espacio-num-cuadro)
   (animacion-func :initarg :animacion-func :initform nil :accessor espacio-animacion-func)
   (datos :initform (list) :accessor espacio-datos)
   (jugando :initform nil :accessor espacio-jugando)
   (terminar :initform nil :accessor espacio-terminar)))

(defmethod compose-space ((pane espacio-pane) &key width height)
  (declare (ignore width height))
  (make-space-requirement :min-width *ancho*
                          :min-height *alto*
                          :width *ancho*
                          :height *alto*
                          :max-width *ancho*
                          :max-height *alto*))

(define-application-frame slime-jam ()
  ((teclas :initarg :teclas :initform nil)
   (hilo-animacion :initform (bt:make-lock) :accessor slime-jam-hilo-animacion)
   (bloqueo :initform (bt:make-lock "Bloqueo para el frame") :accessor slime-jam-bloqueo))
  (:panes (espacio-pane (make-pane 'espacio-pane
                                   :animacion-func nil)))
  (:layouts (:default espacio-pane))
  (:menu-bar t))

(defmethod handle-repaint ((pane espacio-pane) region)
  ;;(declare (ignore region))
  ;;(log:info "region:~s" region)
  (draw-design pane (espacio-buffer pane)
               :clipping-region region))

(defmethod run-frame-top-level :before (frame &key &allow-other-keys)
  (let* ((pane (find-pane-named frame 'espacio-pane))
         (pixmap (allocate-pixmap pane
                                  (bounding-rectangle-width (sheet-region pane))
                                  (bounding-rectangle-height (sheet-region pane)))))
    (setf (espacio-pixmap pane) pixmap)
    (setf (slot-value pixmap 'medium) (make-medium (port pane) pixmap)))
  ;;(setf (slime-jam-hilo-animacion frame) (bt:make-thread (anima frame) :name "Animación"))
  )


(defmethod handle-event ((gadget espacio-pane) (evento key-press-event))
  (when *application-frame*
    (with-slots (escenario) *application-frame*
      (let* ((tecla (keyboard-event-key-name evento)))
        (case tecla
          ((:|Q| :|q|) (when (espacio-jugando gadget)
                         (execute-frame-command *application-frame* `(com-salir))))
          ((:|N| :|n|) (execute-frame-command *application-frame* `(com-reiniciar)))
          ;; ((:up) (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
          ;;          (incf (getf (first (getf (espacio-datos (find-pane-named *slime-jam* 'espacio-pane))
          ;;                                   :slimes-vectoriales))
          ;;                      :y)
          ;;                2)))
          ;; ((:down) (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
          ;;          (decf (getf (first (getf (espacio-datos (find-pane-named *slime-jam* 'espacio-pane))
          ;;                                   :slimes-vectoriales))
          ;;                      :y)
          ;;                2)))
          ;; ((:left) (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
          ;;          (decf (getf (first (getf (espacio-datos (find-pane-named *slime-jam* 'espacio-pane))
          ;;                                   :slimes-vectoriales))
          ;;                      :x)
          ;;                2)))
          ;; ((:right) (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
          ;;             (incf (getf (first (getf (espacio-datos (find-pane-named *slime-jam* 'espacio-pane))
          ;;                                      :slimes-vectoriales))
          ;;                         :x)
          ;;                   2)))
          (t (unless (espacio-jugando gadget)
               (execute-frame-command *application-frame* `(com-jugar)))))))))

(defmethod handle-event ((gadget espacio-pane) (evento key-release-event))
  (when *application-frame*
    (with-slots (escenario) *application-frame*
      (let* ((tecla (keyboard-event-key-name evento)))
        t))))

;;;; Comandos

(define-slime-jam-command (com-jugar :name "Jugar" :menu t)
    ()
  (let ((espacio (find-pane-named *application-frame* 'espacio-pane)))
    (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
      (setf (espacio-datos espacio) nil
            (espacio-jugando espacio) t
            (espacio-animacion-func espacio) #'jugar
            (espacio-num-cuadro espacio) 1))))

(defun inicializa-slime-jam (espacio)
  (let (;;(ancho (bounding-rectangle-width (sheet-region pane)))
        (alto (bounding-rectangle-height (sheet-region espacio))))
    (crea-slime espacio :jugador 64 (- alto 32))
    (crea-slime espacio :enemigo 64 160 t)
    (crea-slime espacio :enemigo 352 160 t)
    (crea-slime-vectorial espacio :tipo-1 256 32 64 nil)))

(defun jugar-slime-jam (frame)
  (loop with pane = (find-pane-named frame 'espacio-pane)
        while (bt:with-lock-held ((slime-jam-bloqueo frame)) (null (espacio-terminar pane)))
          initially (queue-event pane
                                 (make-instance 'window-repaint-event
                                                :region +everywhere+
                                                :sheet pane))
        do (sleep *pausa*)
           (unless (espacio-terminar pane)
             (loop for region in (append (dibuja-slimes pane)
                                         (dibuja-slimes-vectoriales pane))
                   if region do
                     (queue-event pane
                                  (make-instance 'window-repaint-event
                                                 :region region
                                                 :sheet pane))
                   do (bt:thread-yield)))))


(define-slime-jam-command (com-jugar-2 :name "Jugar-2" :menu t)
    ()
  (let ((espacio (find-pane-named *application-frame* 'espacio-pane)))
    (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
      (setf (espacio-datos espacio) nil
            (espacio-jugando espacio) t
            ;;(espacio-animacion-func espacio) #'jugar-2
            (espacio-num-cuadro espacio) 1)
      (dibuja-fondo espacio)
      (inicializa-slime-jam espacio))
    ;;(setf (medium-background espacio) (espacio-imagen espacio))
    (setf (slime-jam-hilo-animacion *application-frame*)
          (bordeaux-threads:make-thread (lambda ()
                                          (jugar-slime-jam *slime-jam*))
                                        :name "com-jugar-2"))))

(define-slime-jam-command (com-reiniciar :name "Reiniciar" :menu t)
    ()
  (let ((espacio (find-pane-named *application-frame* 'espacio-pane)))
    (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
      (setf (espacio-terminar espacio) t
            (espacio-num-cuadro espacio) 1
            (espacio-datos espacio) nil))))

(define-slime-jam-command (com-salir :name "Salir" :menu t)
    ()
  (bt:with-lock-held ((slime-jam-bloqueo *application-frame*))
    (let ((espacio (find-pane-named *application-frame* 'espacio-pane)))
      (setf (espacio-terminar espacio) t)
      (frame-exit *application-frame*))))


(defun slime-jam-entry-point ()
  (apply 'main uiop:*command-line-arguments*))
