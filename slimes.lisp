(in-package #:slime-jam)

(trivial-clim-sprites:load-textures)

(defparameter *slime-ataque*
  (trivial-clim-sprites:make-sprite :slime :attack
                                    :frames-to-advance 6))

(defparameter *slime-tipos*
  (list :enemigo (list :sprite '(:nombre :drop :accion :fall :cuadros 6)
                       :estados '(:inicial (:fall . 0)
                                  :cayendo :fall))
        :jugador (list :sprite '(:nombre :slime :accion :walk :cuadros 3)
                       :estados '(:inicial (:walk . 0)
                                  :caminando :walk
                                  :atacando :attack))))

(defun crea-slime (pane tipo &optional (x 0) (y 0) animado-p)
  "Makes a new slime of the type TIPO; X and Y are the coordinates of the lower left corner."
  (let* ((tipo (getf *slime-tipos* tipo))
         (sprite-tipo (and tipo (getf tipo :sprite))))
    (when sprite-tipo
      (let ((slime (list :x (or x 0)
                         :y (or y 0)
                         :x-anterior nil
                         :y-anterior nil
                         :sprite (make-sprite (getf sprite-tipo :nombre)
                                              (getf sprite-tipo :accion)
                                              :frames-to-advance (getf sprite-tipo :cuadros))
                         :tipo tipo
                         :estado (if animado-p :animado :inicial)
                         :volteado nil)))
        (push slime (getf (espacio-datos pane) :slimes))
        (dibuja-slime pane slime)))))

(defun slime-cambia-estado (slime &optional estado)
  (let* ((tipo (getf slime :tipo))
         (nuevo-estado (or estado (let* ((estados (getf tipo :estados))
                                         (actual-pos (position (getf slime :estado) estados)))
                                    (if (<= actual-pos (- (length estados) 3))
                                        (nth (+ 2 actual-pos) estados)
                                        (car estados))))))
    (if (eq :animado estado)
        (setf (getf slime :estado) nuevo-estado)
        (setf (getf slime :estado) nuevo-estado
              (sprite-action (getf slime :sprite))
              (let ((accion (getf (getf tipo :estados) nuevo-estado)))
                (if (keywordp accion)
                    accion
                    (car accion)))))))

(defun slime-voltea (slime)
  (setf (getf slime :volteado) (not (getf slime :volteado))))

(defun slime-camina (slime)
  (slime-cambia-estado slime :caminando))

(defun slime-ataca (slime slimes)
  (declare (ignore slimes))
  (slime-cambia-estado slime :atacando))

(defun dibuja-slime (pane slime)
  "Draw a slime on the pane PANE."
  (let ((sprite (getf slime :sprite))
        (estado (getf slime :estado)))
    (if (eq :animado estado)
        (sprite-draw-next-frame (espacio-buffer pane) sprite
                                (getf slime :x) (- (getf slime :y) (sprite-height sprite))
                                (espacio-imagen pane)
                                (getf slime :volteado))
        (when (or (null (getf slime :x-anterior))
                  (/= (getf slime :x) (getf slime :x-anterior))
                  (/= (getf slime :y) (getf slime :y-anterior)))
          (setf (getf slime :x-anterior) (getf slime :x)
                (getf slime :y-anterior) (getf slime :y))
          (sprite-draw-frame (espacio-buffer pane) sprite
                             (getf slime :x) (- (getf slime :y) (sprite-height sprite))
                             nil
                             ;;(cdr (getf (getf (getf *slime-tipos* (getf slime :tipo)) :estados) estado))
                             (espacio-imagen pane)
                             (getf slime :volteado))))))

(defun dibuja-slimes (pane)
  "Draw all the slimes on the pane."
  (mapcar (lambda (slime)
            (dibuja-slime pane slime))
          (getf (espacio-datos pane) :slimes)))
;;;;
