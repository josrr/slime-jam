;;;; slime-jam.asd

(asdf:defsystem #:slime-jam
  :description "Describe slime-jam here"
  :author "José Miguel Ángel Ronquillo Rivera"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (#:uiop #:alexandria #:log4cl #:bordeaux-threads
               #:mcclim #:mcclim-raster-image #:trivial-clim-sprites
               #:3d-vectors #:3d-matrices)
  :components ((:file "package")
               (:file "parametros")
               (:file "presentacion")
               (:file "escenarios")
               (:file "slimes-vectoriales")
               (:file "slimes")
               (:file "animaciones")
               (:file "slime-jam")))
